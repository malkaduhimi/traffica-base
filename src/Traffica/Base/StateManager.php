<?php namespace Traffica\Base;

class StateManager
{
    private static $instance;
    
    private $params = array();
    
    public static function instance()
    {
        if (null === static::$instance) {
            if(session_id() == '') {
                session_start();
            }
        
            static::$instance = new static();
        }
        
        return static::$instance;
    }
    
    protected function __construct(){}
    private function __clone() {}
    private function __wakeup() {}
    
    
    
    public function get($param, $default = false)
    {
        if (isset($this->params[$param])) {
            return $this->params[$param];
        } elseif (empty($_SESSION[$param])) {
            return $default;
        } else {
            return $_SESSION[$param];
        }
    }
    
    public function set($param, $value)
    {
        $_SESSION[$param]     = $value;
        $this->params[$param] = $value;
        
        return $this;
    }
}