<?php namespace Traffica\Base;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

use Illuminate\Events\Dispatcher;

use Clearweb\Clearworks\Clearworks;
use Clearweb\Clearworks\Environment;


class Traffica extends Clearworks
{
    private $registeredContainer = array();
    
	public function __construct(Request $request=null)
	{
		if ( ! empty($request)) {
			$this->register('request', function() use($request) { return $request; });
		}
		
		
	}
	
	public function init()
	{
        if ( ! $this->registered('template')) {
            $this->register('template', function() { return new Templater; });
        }
        
        if ( ! $this->registered('dispatcher')) {
			$this->register('dispatcher', function(){ return new Dispatcher; });
		}
        
        if ( ! $this->registered('state')) {
            $this->register('state', function(){ return StateManager::instance(); });
        }
	}
	
	public function execute()
	{
		if ( ! $this->registered('request')) {
			$this->register('request', function() { return Request::createFromGlobals(); });
		}
		
		$routes = new RouteCollection();
        
        $routes->add(
                     'param',
                     new Route('/traffica-param-change/{param}/{value}')
                     );
        
		$routes->add(
					 'all', 
					 new Route('/{env}/{page}{params}', array('env'=>null, 'page'=>null, 'params' => ''), array('params'=>'/.*'))
					 );
        
		$context = new RequestContext();
		$context->fromRequest($this->request);
		
		$matcher = new UrlMatcher($routes, $context);

        $url = $this->request->getRequestUri();
        
        /* remove query string */
        if(preg_match('#([^?]*)\?.*#', $url, $matches)) {
            $url = $matches[1];
        }
            
		$parameters = $matcher->match($url);
        
        if($parameters['_route'] == 'all') {
            $env    = $parameters['env'];
            $page   = $parameters['page'];
            $params = $parameters['params'];
		
            if( ! empty($parameters['env'])) {
                if($this->hasEnvironmentBySlug($parameters['env'])) {
                    $env  = $parameters['env'];
                } else {
                    $env  = null;
                    $page = $parameters['env'];
				
                    if  ( ! empty($parameters['page'])) {
                        $params = '/'.$parameters['page'].$params;
                    }
                }
            }
		
            if (empty($environment)) {
                $environment = $this->getDefaultEnvironment();
            } else {
                $environment = $this->getEnvironment($env);
            }
		
            $this->setCurrentEnvironment($environment);
		
            $environment->init();
		
            $environment->loadFromUrl($page.$params);
            $environment->execute();
        } elseif($parameters['_route'] == 'param') {
            $this->state->set($parameters['param'], $parameters['value']);
            die;
        }
	}
	
	public function getView() {
		if ($this->getCurrentEnvironment()->foundPage()) {
			echo $this->getCurrentEnvironment()->getView();
		} else {
			echo 'not found!';
		}
	}
	
	

	
	/* -- Missing / overriding Clearworks function -- */
	
	function make($name, $url='page', $default = false) {
		$environment = with(new Environment($this->dispatcher))->setName($name)->setSlug($url);
		
		if ($this->addEnvironment($environment, $default)) {
			return $environment;
		} else {
			return FALSE;
		}
	}
	
	function getEnvironmentBySlug($slug) {
		foreach($this->environments as $environment) {
			if ($environment->getSlug() == $slug) {
				return $environment;
			}
		}

		return false;
	}
	
	public function hasEnvironmentBySlug($slug)
	{
		return ($this->getEnvironmentBySlug($slug) != false);
	}
	
    
    
    /* -- IoC -- */
    
    public function resolve($name)
    {
        if ($this->registered($name)) {
            return $this->registeredContainer[$name]();
        } else {
            throw new IoCNotFoundException('Resolving "'.$name.'" failed');
        }
    }
    
    public function register($name, $callback)
    {
        $this->registeredContainer[$name] = $callback;
        
        return $this;
    }
    
    public function registered($name)
    {
        return isset($this->registeredContainer[$name]);
    }
    
    public function __get($attr)
    {
        return $this->resolve($attr);
    }
    
}