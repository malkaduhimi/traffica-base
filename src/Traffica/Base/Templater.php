<?php namespace Traffica\Base;

class Templater extends \Dwoo\Core
{
    public function __construct()
    {
        $this->setTemplateDir('views/');
        $this->setCompileDir('cache');
    }
    
    public function fileExists($file)
    {
        $exists = false;
        
        foreach($this->getTemplateDir() as $dir) {
            if (file_exists($dir.$file)) {
                $exists = true; 
            }
        }
        
        return $exists;
    }
}