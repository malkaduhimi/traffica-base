<?php namespace Traffica\Base;

use Clearweb\Clearworks\Layout\LinearLayout;

abstract class Layout extends LinearLayout
{
    private $viewFile = '';
    
    public function getViewFile() {
        return $this->viewFile;
    }
    
    public function setViewFile($file) {
        $this->viewFile = $file;
        return $this;
    }
    
	public function getView()
	{
        if ( ! traffica()->template->fileExists($this->getViewFile())) {
            throw new LayoutViewFileException('View file with name "'.$this->getViewFile().'" does not exist');
        }
        
        $params = array();
        
        foreach($this->containers as $container) {
            $params[$container] = $this->getContainerView($container);
        }
        
        $html = '<!DOCTYPE html>'.PHP_EOL;
        
        $scripts = array('dependent'=>array(), 'independent'=>array());
        foreach($this->getScripts() as $script) {
            if (empty($script['dependencies'])) {
                $scripts['independent'][$script['name']] = $script['url'];
            } else {
                $scripts['dependent'][$script['name']] = $script;
            }
        }
        
        $html .= '<head>';
        
        $showScript = function($javascript) {
            if (preg_match('#^https?://.*#', $javascript)) {
                return '<script src="'.$javascript.'"></script>'.PHP_EOL;
            } else {
                return '<script src="/'.$javascript.'"></script>'.PHP_EOL;
            }
        }
        ;
        
        foreach($scripts['independent'] as $script) {
            $html .= $showScript($script);
        }

        foreach($scripts['dependent'] as $script) {
            $html .= $showScript($script['url']);
        }
        
        foreach($this->getStyles() as $style) {
            if (preg_match('#^https?://.*#', $style)) {
                $html .= '<link rel="stylesheet" href="'.$style.'">'.PHP_EOL;
            } else {
                $html .= '<link rel="stylesheet" href="/'.$style.'">'.PHP_EOL;
            }
        }
        
        $html .= '</head>';
        
        $html .= traffica()->template->get($this->getViewFile(), $params);
        return $html;
	}
}