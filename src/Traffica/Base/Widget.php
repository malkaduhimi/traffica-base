<?php namespace Traffica\Base;

use Clearweb\Clearworks\Widget\WidgetInterface;
use Clearweb\Clearworks\Contracts\IExecutable;

class Widget implements WidgetInterface
{
	private $parameters      = array();
    private $scripts         = array();
    private $styles          = array();
    
    private $listenerManager = array();
    
    protected $output        = '';
    
    
    public function __construct()
    {
        $this->setListenerManager(new WidgetListenerManager);
    }
    
	public function init()
	{
        $this->getListenerManager()
            ->setWidget($this)
            ->init()
            ->execute();
            ;
        $this->output .= $this->getListenerManager()->getView();
        
		$this->setStatus(IExecutable::STATUS_INITED)
            ->script('phery', 'vendor/phery/phery/phery.min.js', array('jquery'))
            ->script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.js')
            ->script('traffica_listeners', 'vendor/traffica/base/js/listeners.js')
            ;
        
        return $this;
	}
	
	public function execute()
	{
        $this->output .= $this->build();
		$this->setStatus(IExecutable::STATUS_EXECUTED);
        
        return $this;
	}
	
	public function getView()
	{
		return $this->output;
	}
	
    function script($name, $url, array $dependencies=array())
    {
        return $this->addScript($name, $url, $dependencies);
    }
    
    function addScript($name, $url, array $dependencies=array())
    {
		$this->scripts[$name] = array('name'=>$name, 'url'=>$url, 'dependencies'=>$dependencies);
		return $this;
	}
	
	function addStyle($style)
    {
		$this->styles[] = $style;
		return $this;
	}
    
	public function getStyles()
	{
		return $this->styles;
	}
	
	public function getScripts()
	{
		return $this->scripts;
	}
	
    
    
    
    /* -- Listeners -- */

    public function getListenerManager()
    {
        return $this->listenerManager;
    }
    
    public function setListenerManager(WidgetListenerManager $listenerManager)
    {
        $this->listenerManager = $listenerManager;
    }
    
    /**
     * Gets if the event listener exists
	 * @return boolean
	 */
	public function hasEventListener($name)
    {
        $this->getListenerManager()->hasEventListener($name);
	}
    
	/**
	 * Add an event listener
	 * @param string $name the event name which we will listen to.
	 */
	public function addEventListener($name, $method)
    {
        $this->getListenerManager()->addEventListener($name, $method);
	}
    
	/**
	 * Remove an event listener
	 * @param string $name the name of the event
	 */
	public function removeEventListener($name)
    {
        $this->getListenerManager()->removeEventListener($name);
	}
    
    /**
     * Gets if the param change listener exists
	 * @return boolean
	 */
	public function hasParamListener($name)
    {
        $this->getListenerManager()->hasParamListener($name);
	}
    
	/**
	 * Add a param listener
	 * @param string $name the event name which we will listen to.
	 */
	public function addParamListener($name, $method)
    {
        $this->getListenerManager()->addParamListener($name, $method);
	}
    
	/**
	 * Remove a param listener
	 * @param string $name the name of the event
	 */
	public function removeParamListener($name)
    {
        $this->getListenerManager()->removeParamListener($name);
	}
	
    
    
    
    
	
	/* -- GETTERS AND SETTERS -- */
	
	public function getID() {
		return $this->id;
	}
    
    public function setID($id) {
        $this->id = $id;
        
        return $this;
    }

	public function getName() {
        if (empty($this->name)) {
            throw new NoNameException('Widget '.get_class().' has no name set!');
        }
        
		return $this->name;
	}
    
    public function setName($name) {
        $this->name = $name;
        
        return $this;
    }
	
	
	
	
	
	
	/* -- STATUS -- */
	
	
	
	/**
     * Sets the execution status of the widget
     * @param string $status the status we are currently in
     */
    public function setStatus($status) {
        
        switch($status) {
        case IExecutable::STATUS_START:
        case IExecutable::STATUS_INITED:
        case IExecutable::STATUS_EXECUTED:
            $this->status = $status;
        default:
            $this->status = IEXECUTABLE::STATUS_UNKNOWN;
        }
        
        return $this;
    }
    
    /**
     * Gets the execution status of the widget
     * @return string the status we are currently in
     */
    public function getStatus() {
        return $this->status;
    }
	
	/**
     * Gets if object is inited
     * @return boolean if inited true, false otherwise
     */
    public function isInited()
    {
        return ($this->getStatus() != IExecutable::STATUS_START);
    }
	
	/**
     * Gets if object is executed
     * @return boolean if executed true, false otherwise
     */
    public function isExecuted()
    {
        return ($this->getStatus() == IExecutable::STATUS_EXECUTED);
    }
	
	
	
	
	
	
	

	
	/* -- PARAMETERS -- */
    
    /**
     * Shorthand for getParameter
     */
    public function param($name, $default) {
        return $this->getParameter($name, $default);
    }
	
	/**
	 * Set URL parameters for the widget
	 * @return Widget this widget.
	 */
	function setParameters(array $parameters) {
		$this->parameters = $parameters;
		
		return $this;
	}
	
	/**
	 * Gets the url parameters of the widget
	 */
	function getParameters() {
		return $this->parameters;
	}
	
	/**
	 * Gets an url parameter of the widget
	 * @param string $param the name of the parameter.
	 * @param mixed $default the default value to return if the parameter is not set.
	 * @return string the value of the parameter.
	 */
	function setParameter($param, $value) {
		$this->parameters[$param] = $value;
		
		return $this;
	}
	
	/**
	 * Gets an url parameter of the widget
	 * @param string $param the name of the parameter.
	 * @param mixed $default the default value to return if the parameter is not set.
	 * @return string the value of the parameter.
	 */
	function getParameter($param, $default = null) {
		if (isset($this->parameters[$param]))
			return $this->parameters[$param];
		else
			return $default;
	}
}