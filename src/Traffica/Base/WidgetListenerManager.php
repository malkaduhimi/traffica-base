<?php namespace Traffica\Base;

use Phery\Phery;
use ReflectionClass;

class WidgetListenerManager
{
    private $eventListeners = array();
    private $paramListeners = array();
    private $widget         = null;
    
    public function init()
    {
        $reflection = new ReflectionClass($this->getWidget());
        $methods = $reflection->getMethods();
        
        foreach($methods as $method) {
            $matches = array();
            
            if (preg_match('#^onChange(.+)#', $method->name, $matches)) {
                $param = str_replace('_', '-', snake_case($matches[1]));
                
                if ($method->getNumberOfParameters() != 2) {
                    throw new ListenerArgumentsException('Parameter change listener "'.$method->name.'" should accept two parameters ($old, $new).');
                }
                
                $this->addParamListener($param, $method->name);
            } elseif (preg_match('#^on(.+)#', $method->name, $matches)) {
                $event = str_replace('_', '-', snake_case($matches[1]));
                $this->addEventListener($event, $method->name);
                
                if ($method->getNumberOfParameters() != 1) {
                    throw new ListenerArgumentsException('Event listener "'.$method->name.'" should accept one array parameter (array $data).');
                }
            }
        }
        
        return $this;
    }
    
    public function execute()
    {
        return $this;
    }
    
    public function getView()
    {
        $output = '';
        
        $pheryCallbacks = array();
        
        $output .= '<script>';
        
        foreach($this->paramListeners as $listener) {
            $callback = array($this->getWidget(), $listener['method']);
            
            $pheryCallbacks[$this->getWidget()->getName().'-param-'.$listener['name']] = function($data) use ($callback){
                return call_user_func($callback, $data['old'], $data['new']);
            }
            ;
            
            $output .= "paramListener('{$listener['name']}', '{$this->getWidget()->getName()}');";
        }
        
        foreach($this->eventListeners as $listener) {
            $callback = array($this->getWidget(), $listener['method']);
            
            $pheryCallbacks[$this->getWidget()->getName().'-'.$listener['name']] = function($data) use ($callback){
                return call_user_func($callback, $data);
            }
            ;
            
            $output .= "eventListener('{$listener['name']}', '{$this->getWidget()->getName()}');";
        }
        
        $output .= '</script>';
        
        \Phery::instance()
            ->set($pheryCallbacks)
            ->process()
            ;
        
        return $output;
    }
    
    public function setWidget(Widget $widget)
    {
        $this->widget = $widget;
        
        return $this;
    }

    public function getWidget()
    {
        return $this->widget;
    }
    
    /**
     * Gets if the event listener exists
	 * @return boolean
	 */
	public function hasEventListener($name)
    {
        $hasListener = false;
        
        foreach($this->eventListeners as $i => $listener) {
            if( $listener['name'] == $name) {
                $hasListener = true;
            }
        }
        
        return $hasListener;
	}
    
    /**
     * Gets the event listener
	 * @return array listener
	 */
	public function getEventListener($name)
    {
        foreach($this->eventListeners as $i => $listener) {
            if( $listener['name'] == $name) {
                return $listener;
            }
        }
        
        return false;
	}
    
	/**
	 * Gets the events this widget listens to.
	 * @return array an array with event names as strings
	 */
	public function getEventListeners()
    {
		return array_map(function($listener) { return $listener['name']; }, $this->eventListeners);
	}
    
	/**
	 * Add an event listener
	 * @param string $name the event name which we will listen to.
	 */
	public function addEventListener($name, $method)
    {
		$this->eventListeners[] = array('name'=>$name, 'method'=>$method);
		return $this;
	}
    
	/**
	 * Remove an event listener
	 * @param string $name the name of the event
	 */
	public function removeEventListener($name)
    {
        foreach($this->eventListeners as $i => $listener) {
            if( $listener['name'] == $name) {
                unset($this->eventListeners[$i]);
            }
        }
        
		return $this;
	}
    
    
    
    /**
     * Gets if the param change listener exists
	 * @return boolean
	 */
	public function hasParamListener($name)
    {
        $hasListener = false;
        
        foreach($this->paramListeners as $i => $listener) {
            if( $listener['name'] == $name) {
                $hasListener = true;
            }
        }
        
        return $hasListener;
	}
    
    /**
     * Gets the param change listener
	 * @return array listener
	 */
	public function getParamListener($name)
    {
        foreach($this->paramListeners as $i => $listener) {
            if( $listener['name'] == $name) {
                return $listener;
            }
        }
        
        return false;
	}
    
    /**
     * Gets the params this widget listens to.
	 * @return array an array with param names as strings
	 */
	public function getParamListeners()
    {
		return array_map(function($listener) { return $listener['name']; }, $this->paramListeners);
	}
    
	/**
	 * Add a param listener
	 * @param string $name the event name which we will listen to.
	 */
	public function addParamListener($name, $method)
    {
		$this->paramListeners[] = array('name'=>$name, 'method'=>$method);
		return $this;
	}
    
	/**
	 * Remove a param listener
	 * @param string $name the name of the event
	 */
	public function removeParamListener($name)
    {
        foreach($this->paramListeners as $i => $listener) {
            if( $listener['name'] == $name) {
                unset($this->paramListeners[$i]);
            }
        }
        
		return $this;
	}
}