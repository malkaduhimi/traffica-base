<?php namespace Traffica\Base;

use Clearweb\Clearworks\Page\LinearPage;
use Clearweb\Clearworks\Widget\WidgetInterface;

class Page extends LinearPage
{
    private $scripts = array();
    
    public function widget(WidgetInterface $widget, $container) {
        return $this->addWidgetLinear($widget, $container);
    }
    
    public function script($name, $url, array $dependencies=array())
    {
        return $this->addScript($name, $url, $dependencies);
    }
    
    public function style($file)
    {
        return $this->addStyle($file);
    }
}