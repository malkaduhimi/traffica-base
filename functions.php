<?php

use Traffica\Base\Traffica;

/*
 * global traffica function 
 */
function traffica($container = null)
{
    global $traffica;
    
    if ( ! isset($traffica)) {
        $traffica = new Traffica;
    }
    
    if ( ! empty($container)) {
        return $traffica->resolve($container);
    } else {
        return $traffica;
    }
}