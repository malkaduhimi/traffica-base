eventListeners = {};
paramListeners = {};
params = {};

function initParams(initialParams) {
    params = initialParams;
}

function eventListener(eventName, widgetName)
{
    if (typeof(paramListeners[eventName]) == 'undefined') {
	eventListeners[eventName] = [widgetName];
    } else {
	eventListeners[eventName].push(widgetName);
    }
}

function paramListener(paramName, widgetName)
{
    if (typeof(paramListeners[paramName]) == 'undefined') {
	paramListeners[paramName] = [widgetName];
    } else {
	paramListeners[paramName].push(widgetName);
    }
}

function event(eventName, data)
{
    if (typeof(eventListeners[eventName]) != 'undefined') {
	$.each(eventListeners[eventName], function(key, widgetName) {
	    phery.remote(widgetName + '-' + eventName, data);
	});
    }
}

function param(paramName, value)
{
    if (typeof(params[paramName]) == 'undefined') {
	oldParam = null;
    } else {
	oldParam = params[paramName];
    }
    
    params[paramName] = value;
    
    $.get(
	'/traffica-param-change/' + paramName + '/' + value, 
	function() {
	    if (typeof(paramListeners[paramName]) != 'undefined') {
		$.each(paramListeners[paramName], function(key, widgetName) {
		    phery.remote(widgetName + '-param-' + paramName, {"old": oldParam, "new": value});
		});
	    }
	}
    );
}